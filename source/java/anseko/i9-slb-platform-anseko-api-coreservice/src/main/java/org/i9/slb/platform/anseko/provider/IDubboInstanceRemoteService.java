package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceListDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceSearchDto;

import java.util.List;

/**
 * 实例远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/4 10:37
 */
public interface IDubboInstanceRemoteService {

    /**
     * 获取实例信息
     *
     * @param id
     * @return
     */
    DubboResult<InstanceDto> getInstanceDto(String id);

    /**
     * 获取实例列表
     *
     * @return
     */
    DubboResult<List<InstanceDto>> getInstanceDtos();

    /**
     * 创建实例信息
     *
     * @param instanceDto
     */
    DubboResult<?> createInstanceInfo(InstanceDto instanceDto);

    /**
     * 更新实例信息
     *
     * @param instanceDto
     */
    DubboResult<?> updateInstanceInfo(InstanceDto instanceDto);

    /**
     * 获取实例列表分页
     *
     * @param instanceSearchDto
     * @return
     */
    DubboResult<InstanceListDto> getInstanceDtosPage(InstanceSearchDto instanceSearchDto);

    DubboResult<?> updateInstanceStatus(String instanceId, Integer status);

    DubboResult<List<InstanceDto>> getIstanceDtosOnline();
}
