package org.i9.slb.platform.anseko.provider.dto;

public class UserPasswordDto implements java.io.Serializable {

    private static final long serialVersionUID = -8984862436274517685L;

    private String userId;

    private String password;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
