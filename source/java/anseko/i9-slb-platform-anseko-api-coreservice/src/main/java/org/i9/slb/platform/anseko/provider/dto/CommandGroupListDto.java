package org.i9.slb.platform.anseko.provider.dto;

import java.util.List;

/**
 * 命令调度器类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:52
 */
public class CommandGroupListDto implements java.io.Serializable {

    private static final long serialVersionUID = 1507638929002757641L;

    private List<CommandGroupDto> list;

    private int totalRow;

    public List<CommandGroupDto> getList() {
        return list;
    }

    public void setList(List<CommandGroupDto> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
