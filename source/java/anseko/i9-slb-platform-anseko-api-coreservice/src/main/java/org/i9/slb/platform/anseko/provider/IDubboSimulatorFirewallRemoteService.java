package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.dto.SimulatorFirewallDto;

/**
 * 模拟器端口映射远程服务
 *
 * @author tao.jiang03@ucarinc.com
 * @version 1.0
 * @date 2019/3/27 16:17
 */
public interface IDubboSimulatorFirewallRemoteService {

    /**
     * 创建模拟器端口映射
     *
     * @param simulatorFirewallDto
     */
    void createSimulatorFirewall(SimulatorFirewallDto simulatorFirewallDto);
}
