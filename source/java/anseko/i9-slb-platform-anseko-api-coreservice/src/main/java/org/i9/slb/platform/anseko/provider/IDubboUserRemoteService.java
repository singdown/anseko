package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.provider.dto.UserDto;
import org.i9.slb.platform.anseko.provider.dto.UserListDto;
import org.i9.slb.platform.anseko.provider.dto.UserPasswordDto;
import org.i9.slb.platform.anseko.provider.dto.UserSearchDto;

/**
 * 用户远程调用服务类
 *
 * @author R12
 * @date 2018年9月4日 10:38:37
 */
public interface IDubboUserRemoteService {
    /**
     * 通过用户名获取用户信息
     *
     * @param username
     * @return
     */
    DubboResult<UserDto> getUserDtoByUsername(String username);

    /**
     * 检查用户名是否已存在
     *
     * @param username
     * @return
     */
    DubboResult<?> checkUsernameAlreadyExist(String username);

    /**
     * 通过编号获取用户信息
     *
     * @param id
     * @return
     */
    DubboResult<UserDto> getUserDtoById(String id);

    /**
     * 获取用户列表
     *
     * @param userSearchDto
     * @return
     */
    DubboResult<UserListDto> getUserDtos(UserSearchDto userSearchDto);

    /**
     * 创建用户
     *
     * @param userDto
     */
    DubboResult<?> createUserInfo(UserDto userDto);

    /**
     * 更新用户
     *
     * @param userDto
     */
    DubboResult<?> updateUserInfo(UserDto userDto);

    /**
     * 删除用户信息
     *
     * @param id
     */
    DubboResult<?> deleteUserById(String id);

    /**
     * 更新用户密码
     *
     * @param userPasswordDto
     * @return
     */
    DubboResult<?> changeUserPassword(UserPasswordDto userPasswordDto);
}
