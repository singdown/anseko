package org.i9.slb.platform.anseko.provider.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 将异常转换为string
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/31 10:33
 */
public class ExceptionUtils {

    public static String exceptionToString(Exception e) {
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter, true));
        return stringWriter.toString();
    }
}
