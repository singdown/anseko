package org.i9.slb.platform.anseko.provider.task;

import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.result.CommandExecuteReDto;
import org.i9.slb.platform.anseko.provider.utils.ShellCommandUtil;

import java.util.concurrent.Callable;

/**
 * shell命令执行任务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:12
 */
public class ShellCommandExecuteTask implements Callable<DubboResult<CommandExecuteReDto>> {

    private ShellCommandParamDto shellCommandParamDto;

    public ShellCommandExecuteTask(ShellCommandParamDto shellCommandParamDto) {
        this.shellCommandParamDto = shellCommandParamDto;
    }

    @Override
    public DubboResult<CommandExecuteReDto> call() throws Exception {
        DubboResult<CommandExecuteReDto> dubboResult = new DubboResult<CommandExecuteReDto>();
        try {
            CommandExecuteReDto commandExecuteReDto = ShellCommandUtil.shellExecuteLocal(shellCommandParamDto);
            dubboResult.setRe(commandExecuteReDto);
        } catch (BusinessException e) {
            dubboResult.setResult(ErrorCode.BUSINESS_EXCEPTION);
            dubboResult.setMessage(e.getMessage());
        } catch (Exception e) {
            dubboResult.setResult(ErrorCode.UNKOWN_ERROR);
        }
        return dubboResult;
    }
}
