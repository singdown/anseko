package org.i9.slb.platform.anseko.vnc.client.rfb.message.writer;

import org.apache.log4j.Logger;
import org.i9.slb.platform.anseko.vnc.client.iostream.output.RfbOutputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.connection.ConnectionParameter;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Encodings;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.MessageTypes;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.PixelFormat;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.VncClientRfbProtocolDecoder;

/**
 * rfb协议vnc客户端消息写入
 *
 * @author r12
 * @date 2019年2月1日 10:22:34
 */
abstract public class VncClientMessageWriter {

    private static final Logger LOGGER = Logger.getLogger(VncClientMessageWriter.class);

    private ConnectionParameter connectionParameter;

    private RfbOutputStream outputStream;

    protected VncClientMessageWriter(ConnectionParameter connectionParameter, RfbOutputStream outputStream) {
        this.connectionParameter = connectionParameter;
        this.outputStream = outputStream;
    }

    public abstract void writeClientInit(boolean shared);

    public abstract void writeStartMessage(int type);

    public abstract void writeEndMessage();

    public void writeSetPixelFormat(PixelFormat pixelFormat) {
        this.writeStartMessage(MessageTypes.setPixelFormat);
        this.outputStream.pad(3);
        pixelFormat.write(this.outputStream);
        this.writeEndMessage();
    }

    public void writeSetEncodings(int nEncodings, int[] encodings) {
        this.writeStartMessage(MessageTypes.setEncodings);
        this.outputStream.skip(1);
        this.outputStream.writeU16(nEncodings);
        for (int i = 0; i < nEncodings; i++) {
            this.outputStream.writeU32(encodings[i]);
        }
        this.writeEndMessage();
    }

    public void writeSetEncodings(int preferredEncoding, boolean useCopyRect) {
        int nEncodings = 0;
        int[] encodings = new int[Encodings.MAX + 2];
        if (connectionParameter.isSupportsLocalCursor()) {
            encodings[nEncodings++] = Encodings.pseudoEncodingCursor;
        }
        if (connectionParameter.isSupportsDesktopResize()) {
            encodings[nEncodings++] = Encodings.pseudoEncodingDesktopSize;
        }
        if (VncClientRfbProtocolDecoder.supported(preferredEncoding)) {
            encodings[nEncodings++] = preferredEncoding;
        }
        if (useCopyRect) {
            encodings[nEncodings++] = Encodings.COPY_RECT;
        }
        for (int i = Encodings.MAX; i >= 0; i--) {
            if (i != preferredEncoding && VncClientRfbProtocolDecoder.supported(i)) {
                encodings[nEncodings++] = i;
            }
        }
        writeSetEncodings(nEncodings, encodings);
    }

    public void writeFramebufferUpdateRequest(int x, int y, int w, int h, boolean incremental) {
        this.writeStartMessage(MessageTypes.framebufferUpdateRequest);
        this.outputStream.writeU8(incremental ? 1 : 0);
        this.outputStream.writeU16(x);
        this.outputStream.writeU16(y);
        this.outputStream.writeU16(w);
        this.outputStream.writeU16(h);
        this.writeEndMessage();
    }

    /**
     * 封装键盘点击消息
     *
     * @param key
     * @param down
     */
    public void writeKeyEvent(int key, boolean down) {
        this.writeStartMessage(MessageTypes.keyEvent);
        this.outputStream.writeU8(down ? 1 : 0);
        this.outputStream.pad(2);
        this.outputStream.writeU32(key);
        this.writeEndMessage();
        LOGGER.info("发送键盘事件, key : " + key + ", down :" + down);
    }

    /**
     * 封装鼠标点击消息
     *
     * @param x
     * @param y
     * @param buttonMask
     */
    public void writePointerEvent(int x, int y, int buttonMask) {
        if (x < 0) {
            x = 0;
        }
        if (y < 0) {
            y = 0;
        }
        if (x >= connectionParameter.getWidth()) {
            x = connectionParameter.getWidth() - 1;
        }
        if (y >= connectionParameter.getHeight()) {
            y = connectionParameter.getHeight() - 1;
        }
        this.writeStartMessage(MessageTypes.pointerEvent);
        this.outputStream.writeU8(buttonMask);
        this.outputStream.writeU16(x);
        this.outputStream.writeU16(y);
        this.writeEndMessage();
        LOGGER.info("发送鼠标事件, x : " + x + ", y :" + y + ", buttonMask : " + buttonMask);
    }

    /**
     * 封装客户端剪切文本
     *
     * @param str
     */
    public void writeClientCutText(String str) {
        this.writeStartMessage(MessageTypes.clientCutText);
        this.outputStream.pad(3);
        this.outputStream.writeString(str);
        this.writeEndMessage();
        LOGGER.info("发送客户端剪贴板, str : " + str);
    }

    public RfbOutputStream getOutputStream() {
        return outputStream;
    }
}
