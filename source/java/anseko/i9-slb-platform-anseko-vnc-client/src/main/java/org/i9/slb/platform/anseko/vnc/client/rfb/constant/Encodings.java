package org.i9.slb.platform.anseko.vnc.client.rfb.constant;

/**
 * vnc协议
 */
public class Encodings {

    public static final int RAW = 0;
    public static final int COPY_RECT = 1;
    public static final int RRE = 2;
    public static final int CO_RRE = 4;
    public static final int HEXTILE = 5;
    public static final int ZRLE = 16;
    public static final int MAX = 255;

    public static final int pseudoEncodingCursor = 0xffffff11;
    public static final int pseudoEncodingDesktopSize = 0xffffff21;

    public static int valueOf(String name) {
        if (name.equalsIgnoreCase("RAW")) {
            return RAW;
        }
        if (name.equalsIgnoreCase("COPY_RECT")) {
            return COPY_RECT;
        }
        if (name.equalsIgnoreCase("RRE")) {
            return RRE;
        }
        if (name.equalsIgnoreCase("CO_RRE")) {
            return CO_RRE;
        }
        if (name.equalsIgnoreCase("HEXTILE")) {
            return HEXTILE;
        }
        if (name.equalsIgnoreCase("ZRLE")) {
            return ZRLE;
        }
        return -1;
    }

    public static String valueOf(int num) {
        switch (num) {
            case RAW:
                return "RAW";
            case COPY_RECT:
                return "COPY_RECT";
            case RRE:
                return "RRE";
            case CO_RRE:
                return "CoRRE";
            case HEXTILE:
                return "HEXTILE";
            case ZRLE:
                return "ZRLE";
            default:
                return "[unknown encoding]";
        }
    }
}
