package org.i9.slb.platform.anseko.vnc.client.rfb.constant;

public class MessageTypes {

    // server to client
    public static final int framebufferUpdate = 0;
    public static final int setColourMapEntries = 1;
    public static final int bell = 2;
    public static final int serverCutText = 3;

    // client to server
    public static final int setPixelFormat = 0;
    public static final int fixColourMapEntries = 1;
    public static final int setEncodings = 2;
    public static final int framebufferUpdateRequest = 3;
    public static final int keyEvent = 4;
    public static final int pointerEvent = 5;
    public static final int clientCutText = 6;
}
