package org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.impl;

import org.apache.log4j.Logger;
import org.i9.slb.platform.anseko.vnc.client.exception.RfbProtocolException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Encodings;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.MessageTypes;
import org.i9.slb.platform.anseko.vnc.client.rfb.handler.VncClientMessageHandler;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.VncClientMessageReader;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.PixelFormat;

/**
 * rfb协议vnc客户端消息读取(v3)
 *
 * @author r12
 * @date 2019年2月1日 10:22:34
 */
public class VncClientMessageReaderV3 extends VncClientMessageReader {

    private int nUpdateRectsLeft;

    private final static Logger LOGGER = Logger.getLogger(VncClientMessageReaderV3.class);

    public VncClientMessageReaderV3(VncClientMessageHandler handler, RfbInputStream inputStream) {
        super(handler, inputStream);
    }

    @Override
    public void readServerInit() {
        int width = this.inputStream.readU16();
        int height = this.inputStream.readU16();
        this.handler.setDesktopSize(width, height);

        PixelFormat pixelFormat = new PixelFormat();
        pixelFormat.read(this.inputStream);
        this.handler.setPixelFormat(pixelFormat);

        String name = this.inputStream.readString();
        this.handler.setDesktopName(name);

        readEndMessage();
        this.handler.serverInit();
    }

    @Override
    public void readMessage() {
        if (nUpdateRectsLeft == 0) {
            int type = inputStream.readU8();
            switch (type) {
                case MessageTypes.framebufferUpdate:
                    readFramebufferUpdate();
                    break;
                case MessageTypes.setColourMapEntries:
                    readSetColourMapEntries();
                    break;
                case MessageTypes.bell:
                    readBell();
                    break;
                case MessageTypes.serverCutText:
                    readServerCutText();
                    break;
                default:
                    LOGGER.error("unknown message type " + type);
                    throw new RfbProtocolException("unknown message type");
            }

        } else {

            int x = inputStream.readU16();
            int y = inputStream.readU16();
            int w = inputStream.readU16();
            int h = inputStream.readU16();
            int encoding = inputStream.readU32();

            switch (encoding) {
                case Encodings.pseudoEncodingDesktopSize:
                    handler.setDesktopSize(w, h);
                    break;
                case Encodings.pseudoEncodingCursor:
                    readSetCursor(x, y, w, h);
                    break;
                default:
                    readRect(x, y, w, h, encoding);
                    break;
            }

            nUpdateRectsLeft--;
            if (nUpdateRectsLeft == 0)
                handler.framebufferUpdateEnd();
        }

    }

    void readFramebufferUpdate() {
        this.inputStream.skip(1);
        nUpdateRectsLeft = this.inputStream.readU16();
        readEndMessage();
        handler.framebufferUpdateStart();
    }
}
