package org.i9.slb.platform.anseko.vnc.client.rfb.message.writer.impl;

import org.i9.slb.platform.anseko.vnc.client.iostream.output.RfbOutputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.connection.ConnectionParameter;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.writer.VncClientMessageWriter;

/**
 * rfb协议vnc客户端消息写入(v3)
 *
 * @author r12
 * @date 2019年2月1日 10:22:34
 */
public class VncClientMessageWriterV3 extends VncClientMessageWriter {

    public VncClientMessageWriterV3(ConnectionParameter connectionParameter, RfbOutputStream outputStream) {
        super(connectionParameter, outputStream);
    }

    @Override
    public void writeClientInit(boolean shared) {
        this.getOutputStream().writeU8(shared ? 1 : 0);
        this.writeEndMessage();
    }

    @Override
    public void writeStartMessage(int type) {
        this.getOutputStream().writeU8(type);
    }

    @Override
    public void writeEndMessage() {
        this.getOutputStream().flush();
    }
}
