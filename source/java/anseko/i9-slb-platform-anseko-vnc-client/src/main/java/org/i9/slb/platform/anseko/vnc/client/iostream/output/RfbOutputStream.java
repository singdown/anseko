package org.i9.slb.platform.anseko.vnc.client.iostream.output;

/**
 * rfb协议输出流
 *
 * @auther jiangtao
 * @date 2019/1/23 21:35
 */
public abstract class RfbOutputStream {

    protected byte[] buf;

    protected int ptr;

    protected int end;

    // overrun() is implemented by a derived class to cope with buffer overrun.
    // It ensures there are at least itemSize bytes of buffer space.  Returns
    // the number of items which fit (up to a maximum of nItems).  itemSize is
    // supposed to be "small" (a few bytes).

    protected abstract int overrun(int itemSize, int nItems);

    /**
     * length() returns the length of the stream.
     *
     * @return
     */
    public abstract int length();

    // check() ensures there is buffer space for at least one item of size
    // itemSize bytes.  Returns the number of items which fit (up to a maximum
    // of nItems).

    public final int check(int itemSize, int nItems) {
        if (ptr + itemSize * nItems > end) {
            if (ptr + itemSize > end) {
                return overrun(itemSize, nItems);
            }
            nItems = (end - ptr) / itemSize;
        }
        return nItems;
    }

    public final void check(int itemSize) {
        if (ptr + itemSize > end) {
            overrun(itemSize, 1);
        }
    }

    /**
     * writeU/SN() methods write unsigned and signed N-bit integers.
     *
     * @param u
     */
    public final void writeU8(int u) {
        check(1);
        buf[ptr++] = (byte) u;
    }

    public final void writeU16(int u) {
        check(2);
        buf[ptr++] = (byte) (u >> 8);
        buf[ptr++] = (byte) u;
    }

    public final void writeU32(int u) {
        check(4);
        buf[ptr++] = (byte) (u >> 24);
        buf[ptr++] = (byte) (u >> 16);
        buf[ptr++] = (byte) (u >> 8);
        buf[ptr++] = (byte) u;
    }

    public final void writeS8(int s) {
        writeU8(s);
    }

    public final void writeS16(int s) {
        writeU16(s);
    }

    public final void writeS32(int s) {
        writeU32(s);
    }

    /**
     * writeString() writes a string - a U32 length followed by the data.
     *
     * @param str
     */
    public final void writeString(String str) {
        int len = str.length();
        writeU32(len);
        for (int i = 0; i < len; ) {
            int j = i + check(1, len - i);
            while (i < j) {
                buf[ptr++] = (byte) str.charAt(i++);
            }
        }
    }

    public final void pad(int bytes) {
        while (bytes-- > 0) writeU8(0);
    }

    public final void skip(int bytes) {
        while (bytes > 0) {
            int n = check(1, bytes);
            ptr += n;
            bytes -= n;
        }
    }

    /**
     * writeBytes() writes an exact number of bytes from an array at an offset.
     *
     * @param data
     * @param offset
     * @param length
     */
    public void writeBytes(byte[] data, int offset, int length) {
        int offsetEnd = offset + length;
        while (offset < offsetEnd) {
            int n = check(1, offsetEnd - offset);
            System.arraycopy(data, offset, buf, ptr, n);
            ptr += n;
            offset += n;
        }
    }

    /**
     * writeOpaqueN() writes a quantity without byte-swapping.
     * Because java has no byte-ordering, we just use big-endian.
     *
     * @param u
     */
    public final void writeOpaque8(int u) {
        writeU8(u);
    }

    public final void writeOpaque16(int u) {
        writeU16(u);
    }

    public final void writeOpaque32(int u) {
        writeU32(u);
    }

    public final void writeOpaque24A(int u) {
        check(3);
        buf[ptr++] = (byte) (u >> 24);
        buf[ptr++] = (byte) (u >> 16);
        buf[ptr++] = (byte) (u >> 8);
    }

    public final void writeOpaque24B(int u) {
        check(3);
        buf[ptr++] = (byte) (u >> 16);
        buf[ptr++] = (byte) (u >> 8);
        buf[ptr++] = (byte) u;
    }

    /**
     * flush() requests that the stream be flushed.
     */
    public void flush() {
    }

    // getptr(), getend() and setptr() are "dirty" methods which allow you to
    // manipulate the buffer directly.  This is useful for a stream which is a
    // wrapper around an underlying stream.

    public byte[] getBuf() {
        return buf;
    }

    public void setBuf(byte[] buf) {
        this.buf = buf;
    }

    public int getPtr() {
        return ptr;
    }

    public void setPtr(int ptr) {
        this.ptr = ptr;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }
}
