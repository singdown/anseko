package org.i9.slb.platform.anseko.downstream.dto.param;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;
import java.util.UUID;

/**
 * 文件命令执行传输对象
 *
 * @author R12
 * @date 2018.08.28
 */
public class GroupCommandParamDto extends BaseCommandParamDto implements java.io.Serializable {

    private static final long serialVersionUID = 4606057537000871228L;

    private String groupId;

    private String targetSimId;

    private List<SimpleCommandParamDto> commands;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JSONField(serialize = false)
    public List<SimpleCommandParamDto> getCommands() {
        return commands;
    }

    public void setCommands(List<SimpleCommandParamDto> commands) {
        this.commands = commands;
    }

    public static GroupCommandParamDto build(List<SimpleCommandParamDto> commands) {
        GroupCommandParamDto groupCommandParamDto = new GroupCommandParamDto();
        groupCommandParamDto.setGroupId(UUID.randomUUID().toString());
        groupCommandParamDto.setCommands(commands);
        return groupCommandParamDto;
    }
}
