package org.i9.slb.platform.anseko.downstream.dto.param;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.UUID;

/**
 * 文件命令执行传输对象
 *
 * @author R12
 * @date 2018.08.28
 */
public class FileCommandParamDto extends SimpleCommandParamDto implements java.io.Serializable {

    public static FileCommandParamDto build(String filePath, String fileContent, String commandLine) {
        FileCommandParamDto fileCommandParamDto = new FileCommandParamDto();
        fileCommandParamDto.setCommandId(UUID.randomUUID().toString());
        fileCommandParamDto.setFilePath(filePath);
        fileCommandParamDto.setFileContent(fileContent);
        fileCommandParamDto.setCommandLine(commandLine);
        return fileCommandParamDto;
    }

    private static final long serialVersionUID = 8260845880438948483L;

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 文件内容
     */
    private String fileContent;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @JSONField(serialize = false)
    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }
}
