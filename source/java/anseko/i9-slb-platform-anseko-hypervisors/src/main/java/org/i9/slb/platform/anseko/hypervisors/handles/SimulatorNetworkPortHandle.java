package org.i9.slb.platform.anseko.hypervisors.handles;

import org.i9.slb.platform.anseko.hypervisors.BaseSimulatorHandle;
import org.i9.slb.platform.anseko.hypervisors.service.IDownStreamRemoteCommand;

/**
 * 虚拟化操作类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:48
 */
public abstract class SimulatorNetworkPortHandle extends BaseSimulatorHandle {

    public SimulatorNetworkPortHandle(String simulatorId, IDownStreamRemoteCommand downStreamRemoteCommand) {
        super(simulatorId, downStreamRemoteCommand);
    }

    /**
     * 进行端口映射操作
     *
     * @param typeStr
     * @param remoteAddress
     * @param sport
     * @param remoteAddress
     * @param dport
     */
    public abstract void executePortMappingCommand(String typeStr, String localAddress, Integer sport, String remoteAddress, Integer dport);
}
