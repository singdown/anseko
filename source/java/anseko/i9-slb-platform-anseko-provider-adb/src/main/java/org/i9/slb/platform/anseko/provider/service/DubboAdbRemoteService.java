package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.provider.IDubboAdbRemoteService;
import org.i9.slb.platform.anseko.provider.dto.AdbHostDto;
import org.i9.slb.platform.anseko.provider.utils.ShellUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * adb处理远程调用服务类
 *
 * @author tao.jiang03@ucarinc.com
 * @version 1.0
 * @date 2019/3/25 12:43
 */
@Service
public class DubboAdbRemoteService implements IDubboAdbRemoteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DubboAdbRemoteService.class);
    /**
     * 启动adb服务
     */
    @Override
    public void startAdbServer() {
        ShellUtils.shellExecuteLocal("adb start-server");
    }

    /**
     * 关闭adb服务
     */
    @Override
    public void killAdbServer() {
        ShellUtils.shellExecuteLocal("adb kill-server");
    }

    /**
     * 连接adb客户端
     *
     * @param adbHostDto
     */
    @Override
    public void connectAdbHost(AdbHostDto adbHostDto) {
        ShellUtils.shellExecuteLocal("adb connect " + adbHostDto);
    }

    /**
     * 批量连接adb客户端
     *
     * @param adbHostDtos
     */
    @Override
    public void batchConnectAdbHost(List<AdbHostDto> adbHostDtos) {
        for (AdbHostDto adbClientConnectDto : adbHostDtos) {
            this.connectAdbHost(adbClientConnectDto);
        }
    }

    /**
     * 检查adb客户端状态
     *
     * @param adbHostDto
     * @return
     */
    @Override
    public boolean checkAdbHostConnectStatus(AdbHostDto adbHostDto) {
        return false;
    }

    /**
     * 获取所有adb客户端状态
     *
     * @return
     */
    @Override
    public List<AdbHostDto> getAdbHostConnectStatus() {
        String str = ShellUtils.shellExecuteLocal("adb devices");
        int p = 0;

        LOGGER.info("str : " + str);

        List<AdbHostDto> adbHostDtos = new ArrayList<AdbHostDto>();

        for (String line : str.split("\r\n")) {
            p++;
            if (p == 1) {
                continue;
            }
            if (line.startsWith("*")) {
                continue;
            }
            System.out.println(line);

            AdbHostDto adbHostDto = new AdbHostDto();
            adbHostDto.setHost("127.0.0.1");
            adbHostDto.setPort(5555);
            adbHostDtos.add(adbHostDto);
        }

        return adbHostDtos;
    }
}
