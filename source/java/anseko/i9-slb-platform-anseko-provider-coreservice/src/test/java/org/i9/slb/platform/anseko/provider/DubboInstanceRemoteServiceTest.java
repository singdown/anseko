package org.i9.slb.platform.anseko.provider;

import com.alibaba.fastjson.JSONObject;
import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class DubboInstanceRemoteServiceTest extends JunitBaseTest {

    @Autowired
    private IDubboInstanceRemoteService dubboInstanceRemoteService;

    @Test
    public void testCreateInstanceInfo() {
        InstanceDto instanceDto = new InstanceDto();
        instanceDto.setVirtualType(1);
        instanceDto.setRemoteAddress("61.181.128.231");
        instanceDto.setInstanceName("实例主机2221");
        instanceDto.setId(UUID.randomUUID().toString());
        DubboResult<?> dubboResult = this.dubboInstanceRemoteService.createInstanceInfo(instanceDto);
        System.out.println(JSONObject.toJSONString(dubboResult));
    }

    @Test
    public void testGetInstanceDtos() {
        DubboResult<List<InstanceDto>> dubboResult = this.dubboInstanceRemoteService.getInstanceDtos();
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        for (InstanceDto instanceDto : dubboResult.getRe()) {
            System.out.println(JSONObject.toJSONString(instanceDto));
        }
    }

    @Test
    public void testGetInstanceDto() {
        String id = "ed55c453-6806-4098-86cc-aed569c03382";
        DubboResult<InstanceDto> dubboResult = this.dubboInstanceRemoteService.getInstanceDto(id);
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        System.out.println(JSONObject.toJSONString(dubboResult.getRe()));
    }
}
