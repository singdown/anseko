package org.i9.slb.platform.anseko.provider;

import com.alibaba.fastjson.JSONObject;
import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.provider.dto.UserDto;
import org.i9.slb.platform.anseko.provider.dto.UserListDto;
import org.i9.slb.platform.anseko.provider.dto.UserSearchDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class DubboUserRemoteServiceTest extends JunitBaseTest {

    @Autowired
    private IDubboUserRemoteService dubboUserRemoteService;

    @Test
    public void testGetUserDtos() {
        this.testBatchCreateUserInfo();
        UserSearchDto userSearchDto = new UserSearchDto();
        userSearchDto.setStartPage(1);
        userSearchDto.setPageSize(5);
        DubboResult<UserListDto> dubboResult = this.dubboUserRemoteService.getUserDtos(userSearchDto);
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        for (UserDto userDto : dubboResult.getRe().getList()) {
            System.out.println(JSONObject.toJSONString(userDto));
        }
    }

    @Test
    public void testGetUserDtoById() {
        DubboResult<UserDto> dubboResult = this.dubboUserRemoteService.getUserDtoById("0e289ef4-2359-43da-b4d7-8d401f04c507");
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        System.out.println(JSONObject.toJSONString(dubboResult.getRe()));
    }

    @Test
    public void testGetUserDtoByUsername() {
        DubboResult<UserDto> dubboResult = this.dubboUserRemoteService.getUserDtoByUsername("jiangtao");
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        System.out.println(JSONObject.toJSONString(dubboResult.getRe()));
    }

    @Test
    public void testCreateUserInfo() {
        UserDto userDto = new UserDto();
        userDto.setUsername("jiangtao");
        userDto.setPassword("jiangtao");
        userDto.setNickname("姜涛");
        userDto.setPortraitUrl("http://www.baidu.com/img.png");
        DubboResult<?> dubboResult = this.dubboUserRemoteService.createUserInfo(userDto);
        System.out.println(JSONObject.toJSONString(dubboResult));
    }

    @Test
    public void testBatchCreateUserInfo() {
        this.testCreateUserInfo();
        for (int i = 0; i < 100; i ++) {
            UserDto userDto = new UserDto();
            userDto.setUsername("jiangtao" + i);
            userDto.setPassword("jiangtao");
            userDto.setNickname("姜涛");
            userDto.setPortraitUrl("http://www.baidu.com/img.png");
            DubboResult<?> dubboResult = this.dubboUserRemoteService.createUserInfo(userDto);
            System.out.println(JSONObject.toJSONString(dubboResult));
        }
    }

    @Test
    public void testUpdateUserInfo() {
        UserDto userDto = new UserDto();
        userDto.setId("e8f76414-0e5a-4b38-8cbe-e677213c7cd7");
        userDto.setNickname("小王");
        userDto.setPortraitUrl("http://www.baidu.com/img.png");
        DubboResult<?> dubboResult = this.dubboUserRemoteService.updateUserInfo(userDto);
        System.out.println(JSONObject.toJSONString(dubboResult));
    }

    @Test
    public void testDeleteUserById() {
        String id = "e8f76414-0e5a-4b38-8cbe-e677213c7cd7";
        DubboResult<?> dubboResult = this.dubboUserRemoteService.deleteUserById(id);
        System.out.println(JSONObject.toJSONString(dubboResult));
    }
}
