/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50554
Source Host           : localhost:3306
Source Database       : kp

Target Server Type    : MYSQL
Target Server Version : 50554
File Encoding         : 65001

Date: 2019-02-13 18:15:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_command_dispatch
-- ----------------------------
DROP TABLE IF EXISTS `t_command_dispatch`;
CREATE TABLE `t_command_dispatch` (
  `id` varchar(255) NOT NULL,
  `commandGroupId` varchar(255) DEFAULT NULL,
  `simulatorId` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `success` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_command_execute
-- ----------------------------
DROP TABLE IF EXISTS `t_command_execute`;
CREATE TABLE `t_command_execute` (
  `id` varchar(255) NOT NULL,
  `commandGroupId` varchar(255) DEFAULT NULL,
  `commandId` varchar(255) DEFAULT NULL,
  `commandLine` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `commandResult` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_instance
-- ----------------------------
DROP TABLE IF EXISTS `t_instance`;
CREATE TABLE `t_instance` (
  `id` varchar(255) NOT NULL,
  `instanceName` varchar(255) DEFAULT NULL,
  `virtualType` int(11) DEFAULT NULL,
  `remoteAddress` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_simulator
-- ----------------------------
DROP TABLE IF EXISTS `t_simulator`;
CREATE TABLE `t_simulator` (
  `id` varchar(255) NOT NULL,
  `simulatorName` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `cpunum` int(11) DEFAULT NULL,
  `ramnum` int(11) DEFAULT NULL,
  `powerStatus` int(255) DEFAULT NULL,
  `vncpassword` varchar(255) DEFAULT NULL,
  `vncport` int(11) DEFAULT NULL,
  `androidVersion` varchar(255) DEFAULT NULL,
  `instance` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `portraitUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
