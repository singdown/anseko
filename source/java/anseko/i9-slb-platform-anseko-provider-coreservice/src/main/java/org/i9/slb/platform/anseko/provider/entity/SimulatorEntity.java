package org.i9.slb.platform.anseko.provider.entity;

/**
 * 模拟器实体类
 *
 * @author R12
 * @date 2018年9月4日 10:35:25
 */
public class SimulatorEntity implements java.io.Serializable {

    private static final long serialVersionUID = 4138155377547254688L;
    /**
     * 编号
     */
    private String id;
    /**
     * 模拟器名称
     */
    private String simulatorName;
    /**
     * 创建日期
     */
    private String createDate;
    /**
     * 创建日期
     */
    private String updateDate;
    /**
     * cpu核数
     */
    private Integer cpunum;
    /**
     * 内存
     */
    private Integer ramnum;
    /**
     * 电源状态
     */
    private Integer powerStatus;
    /**
     * 安卓版本号
     */
    private Integer androidVersion;
    /**
     * 所在实例
     */
    private String instanceId;

    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorName() {
        return simulatorName;
    }

    public void setSimulatorName(String simulatorName) {
        this.simulatorName = simulatorName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCpunum() {
        return cpunum;
    }

    public void setCpunum(Integer cpunum) {
        this.cpunum = cpunum;
    }

    public Integer getRamnum() {
        return ramnum;
    }

    public void setRamnum(Integer ramnum) {
        this.ramnum = ramnum;
    }

    public Integer getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(Integer powerStatus) {
        this.powerStatus = powerStatus;
    }

    public Integer getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(Integer androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
