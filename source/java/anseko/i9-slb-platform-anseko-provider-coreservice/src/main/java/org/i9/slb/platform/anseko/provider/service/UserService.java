package org.i9.slb.platform.anseko.provider.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.provider.entity.UserEntity;
import org.i9.slb.platform.anseko.provider.repository.UserRepository;
import org.i9.slb.platform.anseko.provider.repository.querybean.UserQuery;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService {

    @Resource
    private UserRepository userRepository;

    /**
     * 通过用户名查询用户信息
     *
     * @param username
     * @return
     */
    public UserEntity getUserEntityByUsername(String username) {
        UserEntity userEntity = this.userRepository.getUserEntityByUsername(username);
        return userEntity;
    }

    /**
     * 通过id查询用户信息
     *
     * @param id
     * @return
     */
    public UserEntity getUserEntityById(String id) {
        UserEntity userEntity = this.userRepository.getUserEntityById(id);
        if (userEntity == null) {
            throw new BusinessException("用户不存在");
        }
        return userEntity;
    }

    /**
     * 查询所有用户信息
     *
     * @param userQuery
     * @return
     */
    public PageInfo<UserEntity> getUserEntityList(UserQuery userQuery) {
        PageHelper.startPage(userQuery.getStartPage(), userQuery.getPageSize());
        List<UserEntity> list = this.userRepository.getUserEntityList(userQuery);
        PageInfo<UserEntity> pageInfo = new PageInfo<UserEntity>(list);
        return pageInfo;
    }

    /**
     * 保存用户信息
     *
     * @param userEntity
     */
    public void insertUserEntity(UserEntity userEntity) {
        this.userRepository.insertUserEntity(userEntity);
    }

    /**
     * 更新用户信息
     *
     * @param userEntity
     */
    public void updateUserEntity(UserEntity userEntity) {
        this.userRepository.updateUserEntity(userEntity);
    }

    /**
     * 删除用户信息
     *
     * @param id
     */
    public void deleteUserById(String id) {
        this.userRepository.deleteUserById(id);
    }

    /**
     * 统计用户名数量
     *
     * @param username
     * @return
     */
    public int countUserEntityByUsernameNumber(String username) {
        return this.userRepository.countUserEntityByUsernameNumber(username);
    }

    public void updateUserEntityPassword(String userId, String password) {
        this.userRepository.updateUserEntityPassword(userId, password);
    }
}
