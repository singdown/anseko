package org.i9.slb.platform.anseko.provider.repository.querybean;

/**
 * 命令组查询分页
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/27 14:02
 */
public class CommandGroupQuery extends BasePageQuery implements java.io.Serializable {

    private static final long serialVersionUID = -2292272647240943898L;
    /**
     * 模拟器编号
     */
    private String simulatorId;

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }
}
