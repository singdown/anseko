package org.i9.slb.platform.anseko.console.modules.simulator.controller;

import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorDetailsView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorInfoView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorListView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorSaveForm;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorSearch;
import org.i9.slb.platform.anseko.console.modules.simulator.service.SimulatorService;
import org.i9.slb.platform.anseko.console.utils.HttpResponse;
import org.i9.slb.platform.anseko.console.utils.HttpResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 模拟器控制器
 *
 * @author R12
 * @date 2018.08.30
 */
@Controller
@RequestMapping("/simulator")
public class SimulatorController {

    @Autowired
    private SimulatorService simulatorService;

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulatorController.class);

    @RequestMapping(value = "/getSimulatorDetailsInfo/{simulatorId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<SimulatorDetailsView> getSimulatorDetailsInfo(@PathVariable("simulatorId") String simulatorId) {
        SimulatorDetailsView simulatorDetailsView = this.simulatorService.getSimulatorDetailsInfo(simulatorId);
        return HttpResponse.ok(simulatorDetailsView);
    }

    /**
     * 获取模拟器列表
     *
     * @return
     */
    @RequestMapping(value = "/getSimulatorList.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<List<SimulatorInfoView>> getSimulatorList() {
        List<SimulatorInfoView> re = this.simulatorService.getSimulatorList();
        return HttpResponse.ok(re);
    }

    /**
     * 获取模拟器列表
     *
     * @return
     */
    @RequestMapping(value = "/getSimulatorListPage.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<SimulatorListView> getSimulatorListPage(SimulatorSearch simulatorSearch) {
        SimulatorListView re = this.simulatorService.getSimulatorListPage(simulatorSearch);
        return HttpResponse.ok(re);
    }

    /**
     * 获取模拟器信息
     *
     * @param simulatorId
     * @return
     */
    @RequestMapping(value = "/getSimulatorInfo/{simulatorId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<SimulatorInfoView> getSimulatorInfo(@PathVariable("simulatorId") String simulatorId) {
        SimulatorInfoView re = this.simulatorService.getSimulatorInfo(simulatorId);
        return HttpResponse.ok(re);
    }

    /**
     * 启动模拟器
     *
     * @param simulatorId
     * @throws Exception
     */
    @RequestMapping(value = "/start/{simulatorId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult start(@PathVariable("simulatorId") String simulatorId) {
        try {
            this.simulatorService.startSimulator(simulatorId);
            LOGGER.info("启动模拟器, simulatorId : {}, 成功", simulatorId);
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("启动模拟器, simulatorId : {}, 失败", simulatorId, e);
            return HttpResponse.error(e);
        }
    }

    /**
     * 启动模拟器
     *
     * @throws Exception
     */
    @RequestMapping(value = "/batchStart.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult batchStart() {
        this.simulatorService.batchStartSimulator();
        return HttpResponse.ok();
    }

    /**
     * 重启模拟器
     *
     * @param simulatorId
     * @throws Exception
     */
    @RequestMapping(value = "/reboot/{simulatorId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult reboot(@PathVariable("simulatorId") String simulatorId) {
        try {
            this.simulatorService.rebootSimulator(simulatorId);
            LOGGER.info("重启模拟器, simulatorId : {}, 成功", simulatorId);
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("重启模拟器, simulatorId : {}, 失败", simulatorId, e);
            return HttpResponse.error(e);
        }
    }

    /**
     * 重启模拟器
     *
     * @throws Exception
     */
    @RequestMapping(value = "/batchReboot.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult batchReboot() {
        this.simulatorService.batchRebootSimulator();
        return HttpResponse.ok();
    }

    /**
     * 关闭模拟器
     *
     * @param simulatorId
     * @throws Exception
     */
    @RequestMapping(value = "/shutdown/{simulatorId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult shutdown(@PathVariable("simulatorId") String simulatorId) {
        try {
            this.simulatorService.shutdownSimulator(simulatorId);
            LOGGER.info("关闭模拟器,simulatorId : {}, 成功", simulatorId);
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("关闭模拟器, simulatorId : {}, 失败", simulatorId, e);
            return HttpResponse.error(e);
        }
    }

    /**
     * 关闭模拟器
     *
     * @throws Exception
     */
    @RequestMapping(value = "/batchShutdown.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult batchShutdown() {
        this.simulatorService.batchShutdownSimulator();
        return HttpResponse.ok();
    }

    /**
     * 销毁模拟器
     *
     * @param simulatorId
     * @throws Exception
     */
    @RequestMapping(value = "/destroy/{simulatorId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult destroy(@PathVariable("simulatorId") String simulatorId) {
        try {
            this.simulatorService.destroySimulator(simulatorId);
            LOGGER.info("销毁模拟器, simulatorId : {}, 成功", simulatorId);
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("销毁模拟器, simulatorId : {}, 失败", simulatorId, e);
            return HttpResponse.error(e);
        }
    }

    /**
     * 创建模拟器
     *
     * @param simulatorSaveForm
     * @return
     */
    @RequestMapping(value = "/createSimulator.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> createSimulator(SimulatorSaveForm simulatorSaveForm) {
        try {
            this.simulatorService.createSimulator(simulatorSaveForm);
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("创建模拟器失败", e);
            return HttpResponse.error(e);
        }
    }

    /**
     * 开启adb调试端口
     *
     * @param simulatorId
     * @return
     */
    @RequestMapping(value = "/debugPort/{simulatorId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> debugPort(@PathVariable("simulatorId") String simulatorId) {
        try {
            this.simulatorService.debugPort(simulatorId);
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("开启adb调试端口, simulatorId : {}", simulatorId, e);
            return HttpResponse.error(e);
        }
    }
}
