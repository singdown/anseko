package org.i9.slb.platform.anseko.console.modules.instance.service;

import org.i9.slb.platform.anseko.console.modules.instance.bean.*;

import java.util.List;

/**
 * 实例管理服务类
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/21 16:10
 */
public interface InstanceService {
    /**
     * 获取实例列表
     *
     * @return
     */
    List<InstanceInfoView> getInstanceList();

    /**
     * 获取实例信息
     *
     * @param instanceId
     * @return
     */
    InstanceInfoView getInstanceInfo(String instanceId);

    /**
     * 创建实例信息
     *
     * @param instanceSaveForm
     */
    void createInstanceInfo(InstanceSaveForm instanceSaveForm);

    /**
     * 更新实例信息
     *
     * @param instanceUpdateForm
     */
    void updateInstanceInfo(InstanceUpdateForm instanceUpdateForm);

    InstanceListView getInstanceListPage(InstanceSearch instanceSearch);

    void updateInstanceStatus(String instanceId, Integer status);

    List<InstanceInfoView> getIstanceListOnline();
}
