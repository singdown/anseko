package org.i9.slb.platform.anseko.console.modules.user.service;

import org.i9.slb.platform.anseko.console.modules.common.bean.UserLoginView;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserInfoView;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserListView;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserSaveForm;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserSearch;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserUpdateForm;

public interface UserService {

    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @return
     */
    UserLoginView userLogin(String username, String password);

    /**
     * 获取用户列表
     *
     * @param userSearch
     * @return
     */
    UserListView getUserInfoList(UserSearch userSearch);

    /**
     * 获取用户信息
     *
     * @param id
     * @return
     */
    UserInfoView getUserInfo(String id);

    /**
     * 创建用户信息
     *
     * @param userSaveForm
     */
    void createUserInfo(UserSaveForm userSaveForm);

    /**
     * 更新用户信息
     *
     * @param userUpdateForm
     */
    void updateUser(UserUpdateForm userUpdateForm);

    /**
     * 删除用户
     *
     * @param userId
     */
    void deleteUser(String userId);

    /**
     * 更新用户密码
     *
     * @param userId
     * @param password
     */
    void changeUserPassword(String userId, String password);

    void userLogout(String userToken);
}
