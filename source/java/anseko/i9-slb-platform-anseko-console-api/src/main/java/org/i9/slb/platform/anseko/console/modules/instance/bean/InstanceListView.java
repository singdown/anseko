package org.i9.slb.platform.anseko.console.modules.instance.bean;

import org.i9.slb.platform.anseko.common.types.InstanceStatusEnum;
import org.i9.slb.platform.anseko.common.types.VirtualEnum;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;

import java.util.List;

/**
 * 实列信息视图
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/21 16:12
 */
public class InstanceListView implements java.io.Serializable {

    private static final long serialVersionUID = -1479439069669913202L;

    private List<InstanceInfoView> list;

    private int totalRow;

    public List<InstanceInfoView> getList() {
        return list;
    }

    public void setList(List<InstanceInfoView> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
