package org.i9.slb.platform.anseko.console.modules.user.bean;

import java.util.List;

/**
 * 用户信息视图
 *
 * @author jiangtao
 * @date time
 */
public class UserListView implements java.io.Serializable {

    private static final long serialVersionUID = 8476921885189478255L;
    private List<UserInfoView> list;

    private int totalRow;

    public List<UserInfoView> getList() {
        return list;
    }

    public void setList(List<UserInfoView> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
