package org.i9.slb.platform.anseko.console.modules.user.controller;

import com.alibaba.fastjson.JSONObject;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserInfoView;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserListView;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserSaveForm;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserSearch;
import org.i9.slb.platform.anseko.console.modules.user.bean.UserUpdateForm;
import org.i9.slb.platform.anseko.console.modules.user.service.UserService;
import org.i9.slb.platform.anseko.console.modules.user.service.UserTokenRedisService;
import org.i9.slb.platform.anseko.console.utils.HttpResponse;
import org.i9.slb.platform.anseko.console.utils.HttpResult;
import org.i9.slb.platform.anseko.provider.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserTokenRedisService userTokenRedisService;

    /**
     * 更新用户密码
     *
     * @param userId
     * @param password
     * @return
     */
    @RequestMapping(value = "/changeUserPassword.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> changeUserPassword(@RequestParam("userId") String userId,
                                            @RequestParam("password") String password) {
        this.userService.changeUserPassword(userId, password);
        return HttpResponse.ok();
    }

    /**
     * 获取当前登录用户信息
     *
     * @param userToken
     * @return
     */
    @RequestMapping(value = "/getCurrectLoginUserInfo.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<UserInfoView> getCurrectLoginUserInfo(@RequestHeader("userToken") String userToken) {
        String value = this.userTokenRedisService.getUserTokenValue(userToken);
        UserDto userDto = JSONObject.parseObject(value, UserDto.class);
        UserInfoView userInfoView = new UserInfoView();
        userInfoView.copyProperty(userDto);
        return HttpResponse.ok(userInfoView);
    }

    /**
     * 获取用户列表
     *
     * @return
     */
    @RequestMapping(value = "/getUserList.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<UserListView> getUserList(UserSearch userSearch) {
        UserListView userListView = this.userService.getUserInfoList(userSearch);
        return HttpResponse.ok(userListView);
    }

    /**
     * 获取用户信息
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserInfo/{userId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<UserInfoView> getUserInfo(@PathVariable("userId") String userId) {
        UserInfoView userInfoView = this.userService.getUserInfo(userId);
        return HttpResponse.ok(userInfoView);
    }

    /**
     * 创建用户信息
     *
     * @param userSaveForm
     * @return
     */
    @RequestMapping(value = "/createUser.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> createUser(UserSaveForm userSaveForm) {
        this.userService.createUserInfo(userSaveForm);
        return HttpResponse.ok();
    }

    /**
     * 更新用户信息
     *
     * @param userUpdateForm
     * @return
     */
    @RequestMapping(value = "/updateUser.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> updateUser(UserUpdateForm userUpdateForm) {
        this.userService.updateUser(userUpdateForm);
        return HttpResponse.ok();
    }

    /**
     * 删除用户信息
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/deleteUser/{userId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> deleteUser(@PathVariable("userId") String userId) {
        this.userService.deleteUser(userId);
        return HttpResponse.ok();
    }
}
