package org.i9.slb.platform.anseko.console.modules.common.bean;

import org.i9.slb.platform.anseko.console.modules.user.bean.UserInfoView;

/**
 * 用户登录视图
 *
 * @author R12
 * @date 2019年2月21日 11:13:41
 */
public class UserLoginView implements java.io.Serializable {

    private static final long serialVersionUID = 6455061873656778920L;
    /**
     * 用户信息
     */
    private UserInfoView userInfoView;
    /**
     * 用户令牌
     */
    private String userToken;

    public UserInfoView getUserInfoView() {
        return userInfoView;
    }

    public void setUserInfoView(UserInfoView userInfoView) {
        this.userInfoView = userInfoView;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
