package org.i9.slb.platform.anseko.console.modules.common.controller;

import org.i9.slb.platform.anseko.common.types.AndroidVersionEnum;
import org.i9.slb.platform.anseko.common.types.PowerStateEnum;
import org.i9.slb.platform.anseko.common.types.VirtualEnum;
import org.i9.slb.platform.anseko.console.modules.common.bean.EnumView;
import org.i9.slb.platform.anseko.console.utils.HttpResponse;
import org.i9.slb.platform.anseko.console.utils.HttpResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/config")
public class ConfigController {

    @RequestMapping(value = "/getVirtualList.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<List<EnumView>> getVirtualList() {
        List<EnumView> list = new ArrayList<EnumView>();
        for (VirtualEnum virtualEnum : VirtualEnum.values()) {
            EnumView enumView = new EnumView();
            enumView.setValue(virtualEnum.getValue());
            enumView.setName(virtualEnum.name());
            list.add(enumView);
        }
        return HttpResponse.ok(list);
    }

    @RequestMapping(value = "/getPowerStateList.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<List<EnumView>> getPowerStateList() {
        List<EnumView> list = new ArrayList<EnumView>();
        for (PowerStateEnum powerStateEnum : PowerStateEnum.values()) {
            EnumView enumView = new EnumView();
            enumView.setValue(powerStateEnum.ordinal());
            enumView.setName(powerStateEnum.getName());
            list.add(enumView);
        }
        return HttpResponse.ok(list);
    }


    @RequestMapping(value = "/getAndroidVersionList.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<List<EnumView>> getAndroidVersionList() {
        List<EnumView> list = new ArrayList<EnumView>();
        for (AndroidVersionEnum androidVersionEnum : AndroidVersionEnum.values()) {
            EnumView enumView = new EnumView();
            enumView.setValue(androidVersionEnum.getIndex());
            enumView.setName(androidVersionEnum.getName());
            list.add(enumView);
        }
        return HttpResponse.ok(list);
    }
}
