package org.i9.slb.platform.anseko.common.utils;

import java.security.MessageDigest;

/**
 * md5加密算法
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/12 15:58
 */
public class Md5Util {

    public static String MD5(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        byte[] buf = messageDigest.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            if (Integer.toHexString(0xFF & buf[i]).length() == 1) {
                sb.append("0").append(Integer.toHexString(0xFF & buf[i]));
            } else {
                sb.append(Integer.toHexString(0xFF & buf[i]));
            }
        }
        return sb.toString();
    }
}
