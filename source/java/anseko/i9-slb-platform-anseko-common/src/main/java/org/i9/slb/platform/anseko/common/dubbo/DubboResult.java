package org.i9.slb.platform.anseko.common.dubbo;

import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.exception.BusinessException;

/**
 * dubbo返回结果
 *
 * @param <T>
 * @author R12
 * @date 2018.08.28
 */
public class DubboResult<T> implements java.io.Serializable {

    public static <T> DubboResult<T> ok(T re) {
        DubboResult<T> dubboResult = new DubboResult<T>();
        dubboResult.setRe(re);
        return dubboResult;
    }

    public static <T> DubboResult<T> ok() {
        DubboResult<T> dubboResult = new DubboResult<T>();
        return dubboResult;
    }

    public static <T> DubboResult<T> error(BusinessException e) {
        return error(ErrorCode.BUSINESS_EXCEPTION, e.getMessage());
    }

    public static <T> DubboResult<T> error(int result, String message) {
        DubboResult<T> dubboResult = new DubboResult<T>();
        dubboResult.setResult(result);
        dubboResult.setMessage(message);
        return dubboResult;
    }

    public void setBusinessException(BusinessException e) {
        this.result = ErrorCode.BUSINESS_EXCEPTION;
        this.message = e.getMessage();
    }

    private static final long serialVersionUID = -4931935538750234940L;

    public DubboResult() {
        this.result = 0;
        this.message = "";
    }

    public void tryBusinessException() {
        if (this.result != 0) {
            throw new BusinessException(this.message);
        }
    }

    /**
     * 错误码
     */
    private Integer result;

    /**
     * 错误消息
     */
    private String message;

    /**
     * 返回结果
     */
    private T re;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getRe() {
        return re;
    }

    public void setRe(T re) {
        this.re = re;
    }
}
