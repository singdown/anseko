package org.i9.slb.platform.anseko.libs.net.rfb;

import java.io.DataOutput;
import java.io.IOException;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb:
//			RRE, PixelFormat

public class CoRRE extends RRE {

    public CoRRE(int ai[], PixelFormat pixelformat, int i, int j, int k, int l, int i1,
                 int j1, int k1) {
        super(ai, pixelformat, i, j, k, l, i1, j1, k1);
    }

    public CoRRE(int i, int j, int k, int l, PixelFormat pixelformat, int i1, SubRect asubrect[]) {
        super(i, j, k, l, pixelformat, i1, asubrect);
    }

    public void writeData(DataOutput dataoutput)
            throws IOException {
        dataoutput.writeShort(x);
        dataoutput.writeShort(y);
        dataoutput.writeShort(w);
        dataoutput.writeShort(h);
        dataoutput.writeInt(4);
        dataoutput.writeInt(subrects.length);
        writePixel(dataoutput, pixelFormat, bgpixel);
        for (int i = 0; i < subrects.length; i++) {
            writePixel(dataoutput, pixelFormat, subrects[i].pixel);
            dataoutput.writeByte(subrects[i].x);
            dataoutput.writeByte(subrects[i].y);
            dataoutput.writeByte(subrects[i].w);
            dataoutput.writeByte(subrects[i].h);
        }

    }

    public Object clone()
            throws CloneNotSupportedException {
        SubRect asubrect[] = new SubRect[subrects.length];
        for (int i = 0; i < subrects.length; i++) {
            asubrect[i] = new SubRect();
            asubrect[i].pixel = subrects[i].pixel;
            asubrect[i].x = subrects[i].x;
            asubrect[i].y = subrects[i].y;
            asubrect[i].w = subrects[i].w;
            asubrect[i].h = subrects[i].h;
        }

        return new CoRRE(x, y, w, h, pixelFormat, bgpixel, asubrect);
    }
}
