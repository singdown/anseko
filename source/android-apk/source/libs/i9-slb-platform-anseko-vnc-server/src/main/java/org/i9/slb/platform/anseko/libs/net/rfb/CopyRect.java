package org.i9.slb.platform.anseko.libs.net.rfb;

import java.io.DataOutput;
import java.io.IOException;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb:
//			Rect

public class CopyRect extends Rect {

    public int srcX;
    public int srcY;

    public CopyRect(int i, int j, int k, int l, int i1, int j1) {
        super(i, j, k, l);
        srcX = i1;
        srcY = j1;
    }

    public void writeData(DataOutput dataoutput)
            throws IOException {
        dataoutput.writeInt(1);
        dataoutput.writeShort(srcX);
        dataoutput.writeShort(srcY);
    }
}
