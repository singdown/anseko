package org.i9.slb.platform.anseko.libs.net.rfb.server;

import org.i9.slb.platform.anseko.libs.net.rfb.Colour;
import org.i9.slb.platform.anseko.libs.net.rfb.PixelFormat;
import org.i9.slb.platform.anseko.libs.net.rfb.Rect;

import java.io.IOException;

public interface RFBClient {

    public abstract PixelFormat getPixelFormat();

    public abstract String getProtocolVersionMsg();

    public abstract boolean getShared();

    public abstract int getPreferredEncoding();

    public abstract void setPreferredEncoding(int i);

    public abstract int[] getEncodings();

    public abstract void writeFrameBufferUpdate(Rect arect[])
            throws IOException;

    public abstract void writeSetColourMapEntries(int i, Colour acolour[])
            throws IOException;

    public abstract void writeBell()
            throws IOException;

    public abstract void writeServerCutText(String s)
            throws IOException;

    public abstract void setUpdateIsAvailable(boolean flag);

    public abstract void close()
            throws IOException;
}
