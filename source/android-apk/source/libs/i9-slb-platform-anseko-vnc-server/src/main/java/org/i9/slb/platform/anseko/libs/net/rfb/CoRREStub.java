package org.i9.slb.platform.anseko.libs.net.rfb;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Vector;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb:
//			Rect, CoRRE, PixelFormat

public class CoRREStub extends Rect {

    public CoRRE rects[];

    public CoRREStub(int ai[], PixelFormat pixelformat, int i, int j, int k, int l, int i1,
                     int j1, int k1) {
        super(l, i1, j1, k1);
        Vector vector = new Vector();
        if (j1 <= 255 && k1 <= 255) {
            CoRRE corre = new CoRRE(ai, pixelformat, i, j, k, l, i1, j1, k1);
            vector.addElement(corre);
        } else {
            for (int i2 = 0; i2 < k1; i2 += 255) {
                for (int l1 = 0; l1 < j1; l1 += 255) {
                    int j2 = j1 - l1;
                    int k2 = k1 - i2;
                    if (j2 > 255)
                        j2 = 255;
                    if (k2 > 255)
                        k2 = 255;
                    CoRRE corre1 = new CoRRE(ai, pixelformat, i, j, k, l + l1, i1 + i2, j2, k2);
                    vector.addElement(corre1);
                }

            }

        }
        rects = new CoRRE[vector.size()];
        vector.toArray((Object[]) rects);
        count = rects.length;
    }

    public CoRREStub(int i, int j, int k, int l, CoRRE acorre[]) {
        super(i, j, k, l);
        rects = acorre;
    }

    public void writeData(DataOutput dataoutput)
            throws IOException {
        for (int i = 0; i < rects.length; i++)
            rects[i].writeData(dataoutput);

    }

    public void transform(int i, int j) {
        for (int k = 0; k < rects.length; k++)
            rects[k].transform(i, j);

    }

    public Object clone()
            throws CloneNotSupportedException {
        CoRRE acorre[] = new CoRRE[rects.length];
        for (int i = 0; i < rects.length; i++)
            acorre[i] = (CoRRE) rects[i].clone();

        return new CoRREStub(x, y, w, h, acorre);
    }
}
