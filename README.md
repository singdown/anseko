# 大路十二月（鲲鹏云）

####  代码出售及转让相关技术 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0520/184756_d6124eb0_1437763.png "屏幕截图.png")

#### 项目介绍
- 云手机管理平台，基于dubbo进行远程服务调用，并通过虚拟化技术kvm, xen, vmware, virtualbox等，建立androidx86操作系统，对虚拟机进行统计管理，批量增加和销毁模拟器；

- 通过实现RFB协议实现对vncserver的控制，可以用于desktop控制器，编写统一的脚本控制；

#### 运行demo（新版本界面）
http://39.107.85.226:9001

用户名:jiangtao, 密码:jiangtao

商务合作
- 电子邮件：jiangtao19890902@sina.com
- QQ：584379563
- 联系电话：15822613880（姜）

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2018/0912/171035_6d1214b9_1437763.png "QQ截图20180912170945.png")


#### 环境要求

1. zookeeper-3.4.9
2. dubbo
3. jdk1.8
4. tomcat8
5. mysql
6.kvm, xen, vmware, virtualbox任选其一

#### 项目结构

kpclouds
.idea
docs
source
external-source
i9-slb-platform-anseko-android-tools
i9-slb-platform-anseko-android-vncserver
java
i9-slb-platform-anseko-api-adb
i9-slb-platform-anseko-api-coreservice
i9-slb-platform-anseko-api-downstream
i9-slb-platform-anseko-command
i9-slb-platform-anseko-common
i9-slb-platform-anseko-console-api
i9-slb-platform-anseko-console-vncserver
i9-slb-platform-anseko-dubbo-support
i9-slb-platform-anseko-dubbo-tester
i9-slb-platform-anseko-hypervisors
i9-slb-platform-anseko-hypervisors-kvm
i9-slb-platform-anseko-provider-adb
i9-slb-platform-anseko-provider-coreservice
i9-slb-platform-anseko-provider-downstream
i9-slb-platform-anseko-tcp-client
i9-slb-platform-anseko-tcp-commons
i9-slb-platform-anseko-tcp-protobuf
i9-slb-platform-anseko-tcp-server
i9-slb-platform-anseko-vnc-client
i9-slb-platform-anseko-parent.iml
pom.xml
repackage.bat
repackage.sh
wwwroot
.gitignore
README.md

#### 运行界面
![输入图片说明](https://images.gitee.com/uploads/images/2018/1009/174801_b440705d_1437763.png)

#### 启动说明
1. 安装zookeeper
2. 安装虚拟化环境
